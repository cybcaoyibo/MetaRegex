#ifndef DEBUGDUMP_H
#define DEBUGDUMP_H
#include "MetaRegex.h"
#include <iostream>

namespace MetaRegex {
	namespace Dump {
		namespace details {
			template<typename Node,	int n = 0, bool = (n < Node::EpsilonSize)>
			struct DumpEpsilons {
				static void Dump() {}
			};
			template<typename Node, int n>
			struct DumpEpsilons<Node, n, true> {
				static void Dump() {
					if (n) std::cout << ",";
					std::cout << Node::template GetEpsilon<n>::Value;
					DumpEpsilons<Node, n + 1>::Dump();
				}
			};
		}
		template<typename Node>
		void DumpEpsilons() {
			std::cout << "\tEpsilon: ";
			if (Node::EpsilonSize) {
				details::DumpEpsilons<Node>::Dump();
			} else {
				std::cout << "(None)";
			}
			std::cout << std::endl;
		}
		
		namespace details {
			template<typename Node, char c, bool = (Node::template Next<c>::Value > -1)>
			struct DumpJump {
				static void Dump() {}
			};
			template<typename Node, char c>
			struct DumpJump<Node, c, true> {
				static void Dump() {
					std::cout << "\t'" << c << "' -> " << Node::template Next<c>::Value << std::endl;
				}
			};
		}
		template<typename Node, char c>
		void DumpJump() {
			details::DumpJump<Node, c>::Dump();
		}

		namespace details {
			template<typename Node, char c, bool = (c < CHAR_MAX)>
			struct DumpJumps {
				static void Dump() {
					Dump::DumpJump<Node, c>();
				}
			};
			template<typename Node, char c>
			struct DumpJumps<Node, c, true> {
				static void Dump() {
					Dump::DumpJump<Node, c>();
					DumpJumps<Node, c + 1>::Dump();
				}
			};
		}
		template<typename Node>
		void DumpJumps() {
			details::DumpJumps<Node, CHAR_MIN>::Dump();
		}

		namespace details {
			template<typename Closure, int n, bool = (n < Closure::Size)>
			struct DumpClosures {
				static void Dump() {}
			};
			template<typename Closure, int n>
			struct DumpClosures<Closure, n, true> {
				static void Dump() {
					if (n) std::cout << ",";
					std::cout << Closure::template Get<n>::Value;
					DumpClosures<Closure, n + 1>::Dump();
				}
			};
		}
		template<typename Closure>
		void DumpClosures() {
			std::cout << "\tClosure: ";
			details::DumpClosures<Closure, 0>::Dump();
			std::cout << std::endl;
		}

		namespace details {
			template<typename Graph, int n, bool = (n < Graph::Size)>
			struct DumpNFANode {
				static void Dump() {}
			};
			template<typename Graph, int n>
			struct DumpNFANode<Graph, n, true> {
				typedef typename Graph::template Get<n>::Value Node;
				static void Dump() {
					std::cout << "Id = " << Node::Id << ", CanAccept = " << (Node::CanAccept ? "true" : "false") << std::endl;
					Dump::DumpEpsilons<Node>();
					Dump::DumpClosures<typename MetaRegex::details::template EpsilonClosure<Graph, Node::Id>::Value>();
					Dump::DumpJumps<Node>();
					DumpNFANode<Graph, n + 1>::Dump();
				}
			};
		}
		template<typename Graph>
		void DumpNFA() {
			details::DumpNFANode<Graph, 0>::Dump();
		}

		namespace details {
			template<typename DFA, int n, bool = (n < DFA::Size)>
			struct DumpDFANode {
				static void Dump() {}
			};
			template<typename DFA, int n>
			struct DumpDFANode<DFA, n, true> {
				typedef typename DFA::template Get<n>::Value Node;
				static void Dump() {
					std::cout << "Id = " << Node::Id << ", CanAccept = " << (Node::CanAccept ? "true" : "false") << std::endl;
					Dump::DumpClosures<typename Node::Closure>();
					Dump::DumpJumps<Node>();
					DumpDFANode<DFA, n + 1>::Dump();
				}
			};
		}
		template<typename DFA>
		void DumpDFA() {
			details::DumpDFANode<DFA, 0>::Dump();
		}
	}
}

#endif
