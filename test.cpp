#pragma warning(disable:4503)
#include "MetaRegex.h"
#include "DebugDump.h"
#include <vector>
#include <string>

int main() {
	using namespace MetaRegex;
	//Matches any string that contains a number with 3 or more than 3 digits.
	typedef Expr::Sequence<
		Expr::Star<Expr::Single<Chars::Any>>,
		Expr::Min<Expr::Single<Chars::Digit>, 3>,
		Expr::Star<Expr::Single<Chars::Any>>
	> Expr;
	
	typedef Compile<Expr> Compiled;
	
	typedef Matcher<Compiled> Matcher;
	
	std::vector<std::string> TestStrings = {
		"abcd",
		"abc1abc",
		"abc1abc2abc3",
		"abc123abc",
		"abc1234abc",
		"abc12abc",
		"abc123",
		"abc1234",
		"abc12"
	};

	for (const auto &i : TestStrings) {
		std::cout << i << ": " << (Matcher::Match(i.cbegin(), i.cend()) ? "true" : "false") << std::endl;
	}
	return 0;
}
