//MetaRegex: A compile-time regular expression compiler.
//Author:    Yibo Cao
//Date:      5/14/2016

//This program uses template metaprogramming to construct NFA and DFA for regular expressions at compile-time.
//Static state-transition-tables will be populated at compile time for runtime string matching.

#ifndef METAREGEX_H
#define METAREGEX_H
#include <climits>
#include <type_traits>
#include <iterator>

namespace MetaRegex {
	namespace Chars {
		template<char c>
		struct Single {
			template<char input>
			struct Match {
				enum : bool { Value = c == input };
			};
		};

		template<char ...c>
		class Set {
			template<char input, char ...chars>
			struct Internal;
			template<char input, char nowChar, char ...remaining>
			struct Internal<input, nowChar, remaining...> {
				enum : bool { Value = input == nowChar ? true : Internal<input, remaining...>::Value };
			};
			template<char input>
			struct Internal<input> {
				enum : bool { Value = false };
			};
		public:
			template<char input>
			struct Match {
				enum : bool { Value = Internal<input, c...>::Value };
			};
		};

		template<char low, char high>
		struct Range {
			template<char input>
			struct Match {
				enum : bool { Value = input >= low && input <= high };
			};
		};

		template<typename Rule>
		struct Inverse {
			template<char input>
			struct Match {
				enum : bool { Value = !Rule::template Match<input>::Value };
			};
		};

		template<typename ...Rules>
		class Union {
			template<char input, typename...>
			struct Internal;
			template<char input, typename nowRule, typename ...remaining>
			struct Internal<input, nowRule, remaining...> {
				enum : bool { Value = nowRule::template Match<input>::Value ? true : Internal<input, remaining...>::Value };
			};
			template<char input>
			struct Internal<input> {
				enum : bool { Value = false };
			};
		public:
			template<char input>
			struct Match {
				enum : bool { Value = Internal<input, Rules...>::Value };
			};
		};

		struct Any {
			template<char>
			struct Match {
				enum : bool { Value = true };
			};
		};
		
		typedef Range<'0', '9'> Digit;
		typedef Set<' ', '\t', '\n', '\v', '\f', '\r'> WhiteSpace;
		typedef Union<Range<'a', 'z'>, Range<'A', 'Z'>, Range<'0', '9'>, Single<'_'>> Word;
	}

	namespace details {
		////////////////////////
		//      NFA Utils     //
		////////////////////////
		template<int id>
		struct EmptyNFANode {
			enum : int { Id = id };
			enum : bool { CanAccept = false };
			enum : int { EpsilonSize = 0 };
			template<int>
			struct GetEpsilon;
			template<char c>
			struct Next {
				enum : int { Value = -1 };
			};
		};
		
		struct InitialNFAGraph {
			enum : int { Size = 1 };
			template<int, typename = void>
			struct Get;
			template<typename dummy>
			struct Get<0, dummy> {
				typedef EmptyNFANode<0> Value;
			};
		};

		struct InitialNFAGenState {
			typedef InitialNFAGraph Graph;
			enum : int { NowNode = 0 };
		};

		template<typename Graph, typename ...Modifiers>
		struct AppendNode {
		private:
			typedef EmptyNFANode<Graph::Size> EmptyNode;
			template<typename Prev, typename...>
			struct WithModifiers;
			template<typename Prev, typename First, typename ...Remaining>
			struct WithModifiers<Prev, First, Remaining...> {
				typedef typename WithModifiers<typename First::template Do<Prev>, Remaining...>::Value Value;
			};
			template<typename Prev>
			struct WithModifiers<Prev> {
				typedef Prev Value;
			};
		public:
			enum : int { Size = Graph::Size + 1 };
			template<int n, bool = n == Graph::Size>
			struct Get {
				struct Value : Graph::template Get<n>::Value {};
			};
			template<int n>
			struct Get<n, true> {
				struct Value : WithModifiers<EmptyNode, Modifiers...>::Value {};
			};
		};

		template<typename Graph, int id, typename ...Modifiers>
		struct ApplyModifiers {
			enum : int { Size = Graph::Size };
		private:
			typedef typename Graph::template Get<id>::Value Base;
			template<typename Prev, typename...>
			struct WithModifiers;
			template<typename Prev, typename First, typename ...Remaining>
			struct WithModifiers<Prev, First, Remaining...> {
				typedef typename WithModifiers<typename First::template Do<Prev>, Remaining...>::Value Value;
			};
			template<typename Prev>
			struct WithModifiers<Prev> {
				typedef Prev Value;
			};
		public:
			template<int n, bool = n == id>
			struct Get {
				struct Value : Graph::template Get<n>::Value {};
			};
			template<int n>
			struct Get<n, true> {
				struct Value : WithModifiers<Base, Modifiers...>::Value {};
			};
		};
		
		template<int Target>
		struct AddEpsilon {
			template<typename Original>
			struct Do : public Original {
				enum : int { EpsilonSize = Original::EpsilonSize + 1 };
				template<int n, bool = n == Original::EpsilonSize>
				struct GetEpsilon {
					enum : int { Value = Original::template GetEpsilon<n>::Value };
				};
				template<int n>
				struct GetEpsilon<n, true> {
					enum : int { Value = Target };
				};
			};
		};

		template<typename Rule, int Target>
		struct SetNext {
			template<typename Original>
			struct Do : public Original {
				template<char c>
				struct Next {
					enum : int { Value = Rule::template Match<c>::Value ? Target : Original::template Next<c>::Value };
				};
			};
		};

		struct SetAccept {
			template<typename Original>
			struct Do : public Original {
				enum : bool { CanAccept = true };
			};
		};
		
		////////////////////////
		//   NFA Generators   //
		////////////////////////
		template<typename Rule>
		struct NFAGenSingle {
			template<typename PrevGenState>
			struct Do {
				enum : int { NowNode = PrevGenState::Graph::Size };
				struct Graph : AppendNode<
					ApplyModifiers<
						typename PrevGenState::Graph,
						PrevGenState::NowNode,
						SetNext<Rule, PrevGenState::Graph::Size>
					>
				> {};
			};
		};

		template<typename ...Steps>
		class NFAGenChain {
			template<typename PrevGenState, typename...>
			struct Internal;
			template<typename PrevGenState, typename NowStep, typename ...RemainingSteps>
			struct Internal<PrevGenState, NowStep, RemainingSteps...> {
				typedef typename Internal<typename NowStep::template Do<PrevGenState>, RemainingSteps...>::Value Value;
			};
			template<typename PrevGenState>
			struct Internal<PrevGenState> {
				typedef PrevGenState Value;
			};
		public:
			template<typename PrevGenState>
			struct Do : Internal<PrevGenState, Steps...>::Value {};
		};

		template<typename Content>
		struct NFAGenOptional {
			template<typename PrevGenState>
			class Do {
				typedef typename Content::template Do<PrevGenState> ContentResult;
			public:
				enum : int { NowNode = ContentResult::NowNode };
				struct Graph : ApplyModifiers<
					typename ContentResult::Graph,
					PrevGenState::NowNode,
					AddEpsilon<ContentResult::NowNode>
				> {};
			};
		};
		
		template<typename Content, int N>
		class NFAGenRepeatFixed {
			template<typename PrevGenState, int n>
			struct Internal {
				typedef typename Internal<typename Content::template Do<PrevGenState>, n - 1>::Value Value;
			};
			template<typename PrevGenState>
			struct Internal<PrevGenState, 0> {
				typedef PrevGenState Value;
			};
		public:
			template<typename PrevGenState>
			struct Do : Internal<PrevGenState, N>::Value {};
		};
		
		template<typename ...Contents>
		class NFAGenOr {
			template<typename PrevGenState>
			struct Internal {
				enum : int { Source = PrevGenState::NowNode };
				enum : int { FirstAuxSource = PrevGenState::Graph::Size };
				enum : int { Tail = PrevGenState::Graph::Size + sizeof...(Contents) };
				
				template<typename PrevGraph>
				using AddAuxSource = ApplyModifiers<
					AppendNode<PrevGraph>,
					Source,
					AddEpsilon<PrevGraph::Size>
				>;

				template<typename PrevGraph, int N>
				struct AddAuxSources {
					typedef typename AddAuxSources<AddAuxSource<PrevGraph>, N - 1>::Value Value;
				};
				template<typename PrevGraph>
				struct AddAuxSources<PrevGraph, 0> {
					typedef PrevGraph Value;
				};
				
				typedef typename AddAuxSources<typename PrevGenState::Graph, sizeof...(Contents)>::Value AuxSourcesAdded;
				typedef AppendNode<AuxSourcesAdded> TailAdded;
				
				template<typename PrevState>
				using ConnectToTail = ApplyModifiers<
					typename PrevState::Graph,
					PrevState::NowNode,
					AddEpsilon<Tail>
				>;

				template<typename PrevGraph, typename Content, int AuxId>
				class AddContent {
					struct TmpState {
						typedef PrevGraph Graph;
						enum : int { NowNode = FirstAuxSource + AuxId };
					};
					typedef typename Content::template Do<TmpState> ContentAdded;
				public:
					typedef ConnectToTail<ContentAdded> Result;
				};

				template<typename PrevGraph, int AuxId, typename...>
				struct AddContents;
				template<typename PrevGraph, int AuxId, typename NowContent, typename ...Remaining>
				struct AddContents<PrevGraph, AuxId, NowContent, Remaining...> {
					typedef typename AddContents<typename AddContent<PrevGraph, NowContent, AuxId>::Result, AuxId + 1, Remaining...>::Result Result;
				};
				template<typename PrevGraph, int AuxId>
				struct AddContents<PrevGraph, AuxId> {
					typedef PrevGraph Result;
				};

				typedef typename AddContents<TailAdded, 0, Contents...>::Result FinalGraph;

				struct Result {
					typedef FinalGraph Graph;
					enum : int {NowNode = Tail};
				};
			};
		public:
			template<typename PrevGenState>
			struct Do : Internal<PrevGenState>::Result {};
		};
		template<>
		class NFAGenOr<> {
		public:
			template<typename PrevGenState>
			using Do = PrevGenState;
		};
		
		template<typename Content>
		class NFAGenRepeatAny {
			template<typename PrevGenState>
			struct Internal {
				enum : int { Source = PrevGenState::NowNode };
				enum : int { AuxSource = PrevGenState::Graph::Size };
				enum : int { Tail = PrevGenState::Graph::Size + 1 };
				
				typedef ApplyModifiers<
					AppendNode<
						AppendNode<
							typename PrevGenState::Graph
						>
					>,
					Source,
					AddEpsilon<AuxSource>,
					AddEpsilon<Tail>
				> AuxSourceAndTailAdded;
				
				struct TmpState {
					enum : int { NowNode = AuxSource };
					typedef AuxSourceAndTailAdded Graph;
				};

				typedef typename Content::template Do<TmpState> ContentAdded;
				
				typedef ApplyModifiers<
					typename ContentAdded::Graph,
					ContentAdded::NowNode,
					AddEpsilon<Tail>,
					AddEpsilon<AuxSource>
				> ResultGraph;

				struct Result {
					typedef ResultGraph Graph;
					enum : int { NowNode = Tail };
				};
			};
		public:
			template<typename PrevGenState>
			struct Do : Internal<PrevGenState>::Result {};
		};
		
		template<typename Content>
		class NFAGenRepeatNonZero {
			template<typename PrevGenState>
			struct Internal {
				enum : int { Source = PrevGenState::NowNode };
				enum : int { AuxSource = PrevGenState::Graph::Size };
				enum : int { Tail = PrevGenState::Graph::Size + 1 };

				typedef ApplyModifiers<
					AppendNode<
						AppendNode<
							typename PrevGenState::Graph
						>
					>,
					Source,
					AddEpsilon<AuxSource>
				> AuxSourceAndTailAdded;
				
				struct TmpState {
					enum : int { NowNode = AuxSource };
					typedef AuxSourceAndTailAdded Graph;
				};

				typedef typename Content::template Do<TmpState> ContentAdded;
				
				typedef ApplyModifiers<
					typename ContentAdded::Graph,
					ContentAdded::NowNode,
					AddEpsilon<Tail>,
					AddEpsilon<AuxSource>
				> ResultGraph;

				struct Result {
					typedef ResultGraph Graph;
					enum : int { NowNode = Tail };
				};
			};
		public:
			template<typename PrevGenState>
			struct Do : Internal<PrevGenState>::Result {};
		};
		
		template<typename Content, int N>
		class NFAGenRepeatMin {
			template<typename PrevGenState>
			struct Internal {
				using Value = typename NFAGenChain<NFAGenRepeatFixed<Content, N - 1>, NFAGenRepeatNonZero<Content>>::template Do<PrevGenState>;
			};
		public:
			template<typename PrevGenState>
			struct Do : Internal<PrevGenState>::Value {};
		};
		template<typename Content>
		class NFAGenRepeatMin<Content, 0> {
			template<typename PrevGenState>
			struct Internal {
				using Value = typename NFAGenRepeatAny<Content>::template Do<PrevGenState>;
			};
		public:
			template<typename PrevGenState>
			struct Do : Internal<PrevGenState>::Value {};
		};
		
		template<typename Content, int N>
		class NFAGenRepeatMax {
			template<typename PrevGenState>
			struct Internal {
				using Value = typename NFAGenRepeatFixed<NFAGenOptional<Content>, N>::template Do<PrevGenState>;
			};
		public:
			template<typename PrevGenState>
			struct Do : Internal<PrevGenState>::Value {};
		};

		template<typename Content, int Min, int Max>
		class NFAGenRepeatMinMax {
			template<typename PrevGenState>
			struct Internal {
				using Value = typename NFAGenChain<
					NFAGenRepeatFixed<Content, Min>,
					NFAGenRepeatMax<Content, Max - Min>
				>::template Do<PrevGenState>;
			};
		public:
			template<typename PrevGenState>
			struct Do : Internal<PrevGenState>::Value {};
		};
		
		template<typename Generator>
		class NFAFinishGeneration {
			typedef typename Generator::template Do<InitialNFAGenState> State;
		public:
			struct Value : ApplyModifiers<typename State::Graph, State::NowNode, SetAccept> {};
		};

		////////////////////////
		//    Ordered Set     //
		////////////////////////
		struct EmptySet {
			enum : int { Size = 0 };
		};

		template<typename List, int target, int low, int high, bool = int(low) == int(high)>
		struct LowerBound {
			enum : int { Mid = (low + high) / 2 };
			enum : int { MidVal = List::template Get<Mid>::Value };
			enum : int { NewLow = MidVal < target ? (Mid + 1) : low };
			enum : int { NewHigh = MidVal < target ? high : Mid };
			enum : int { Value = LowerBound<List, target, NewLow, NewHigh>::Value };
		};
		template<typename List, int target, int low, int high>
		struct LowerBound<List, target, low, high, true> {
			enum : int { Value = high };
		};

		template<typename Old, int element>
		class SetInsert {
			enum : int { bound = LowerBound<Old, element, 0, Old::Size>::Value };
			template<bool = int(bound) == int(Old::Size), typename = void>
			struct Internal {
				template<bool = Old::template Get<bound>::Value == element, typename = void>
				struct Internal2 {
					enum : int { Size = Old::Size + 1 };
					template<int n>
					struct Case {
						enum : int { Value = n < bound ? 0 : n == bound ? 1 : 2 };
					};
					template<int n, int>
					struct Internal3;
					template<int n>
					struct Internal3<n, 0> {
						enum : int { Value = Old::template Get<n>::Value };
					};
					template<int n>
					struct Internal3<n, 1> {
						enum : int { Value = element };
					};
					template<int n>
					struct Internal3<n, 2> {
						enum : int { Value = Old::template Get<n - 1>::Value };
					};
					template<int n>
					struct Get {
						enum : int { Value = Internal3<n, Case<n>::Value>::Value };
					};
				};
				template<typename dummy>
				struct Internal2<true, dummy> {
					enum : int { Size = Old::Size };
					template<int n>
					struct Get {
						enum : int { Value = Old::template Get<n>::Value };
					};
				};

				enum : int { Size = Internal2<>::Size };
				template<int n>
				struct Get {
					enum : int { Value = Internal2<>::template Get<n>::Value };
				};
			};
			template<typename dummy>
			struct Internal<true, dummy> {
				enum : int { Size = Old::Size + 1 };
				template<int n, bool = n == Old::Size>
				struct Get {
					enum : int { Value = Old::template Get<n>::Value };
				};
				template<int n>
				struct Get<n, true> {
					enum : int { Value = element };
				};
			};
		public:
			enum : int { Size = Internal<>::Size };
			template<int n>
			struct Get {
				enum : int { Value = Internal<>::template Get<n>::Value };
			};
		};

		template<typename A, typename B>
		class SetIsEqual {
			template<bool = int(A::Size) == int(B::Size), typename = void>
			struct Internal1 {
				template<int i, bool = i == A::Size>
				struct Internal2 {
					enum : bool { Value = int(A::template Get<i>::Value) == int(B::template Get<i>::Value) && Internal2<i + 1>::Value };
				};
				template<int i>
				struct Internal2<i, true> {
					enum : bool { Value = true };
				};
				enum : bool { Value = Internal2<0>::Value };
			};
			template<typename dummy>
			struct Internal1<false, dummy> {
				enum : bool { Value = false };
			};
		public:
			enum : bool { Value = Internal1<>::Value };
		};
		
		template<typename A, typename B>
		class SetUnion {
			template<int i, typename Prev, bool = i == B::Size>
			struct Internal {
				typedef typename Internal<i + 1, SetInsert<Prev, B::template Get<i>::Value>>::Value Value;
			};
			template<int i, typename Prev>
			struct Internal<i, Prev, true> {
				typedef Prev Value;
			};
		public:
			typedef typename Internal<0, A>::Value Result;
		};
		
		////////////////////////
		//   Epsilon Closure  //
		////////////////////////
		template<typename Graph, int nodeId>
		class EpsilonClosure {
			struct InitialIsMarked {
				template<int n>
				struct IsMarked {
					enum : bool { Value = false };
				};
			};
			
			template<int id, typename PrevResult, typename PrevIsMarked, bool = !PrevIsMarked::template IsMarked<id>::Value>
			struct Step {
				struct Final {
					typedef PrevResult Result;
					typedef PrevIsMarked IsMarked;
				};
			};
			template<int id, typename PrevResult, typename PrevIsMarked>
			struct Step<id, PrevResult, PrevIsMarked, true> {
				typedef SetInsert<PrevResult, id> TmpResult;
				struct TmpIsMarked {
					template<int n, bool = n == id>
					struct IsMarked {
						enum : bool { Value = PrevIsMarked::template IsMarked<n>::Value };
					};
					template<int n>
					struct IsMarked<n, true> {
						enum : bool { Value = true };
					};
				};
				enum : int { EpsilonSize = Graph::template Get<id>::Value::EpsilonSize };
				
				template<typename _PrevResult, typename _PrevIsMarked, int epsilonId, bool = (epsilonId < EpsilonSize)>
				struct NextSteps {
					typedef _PrevResult Result;
					typedef _PrevIsMarked IsMarked;
				};
				template<typename _PrevResult, typename _PrevIsMarked, int epsilonId>
				struct NextSteps<_PrevResult, _PrevIsMarked, epsilonId, true> {
					struct Tmp : Step<Graph::template Get<id>::Value::template GetEpsilon<epsilonId>::Value, _PrevResult, _PrevIsMarked>::Final {};
					struct Tmp2 : NextSteps<typename Tmp::Result, typename Tmp::IsMarked, epsilonId + 1> {};
					typedef typename Tmp2::Result Result;
					typedef typename Tmp2::IsMarked IsMarked;
				};
				typedef NextSteps<TmpResult, TmpIsMarked, 0> Final;
			};
		public:
			struct Value : Step<nodeId, EmptySet, InitialIsMarked>::Final::Result {};
		};

		template<typename Graph, typename Closure>
		class CanClosureAccept {
			template<int i, bool = i == Closure::Size>
			struct Internal {
				enum : bool { Value = Graph::template Get<Closure::template Get<i>::Value>::Value::CanAccept || Internal<i + 1>::Value };
			};
			template<int i>
			struct Internal<i, true> {
				enum : bool { Value = false };
			};
		public:
			enum : bool { Value = Internal<0>::Value };
		};
		
		////////////////////////
		//   DFA Generation   //
		////////////////////////
		template<typename NFAGraph, typename closure, int id>
		struct EmptyDFANode {
			enum : int { Id = id };
			typedef closure Closure;
			enum : bool { CanAccept = CanClosureAccept<NFAGraph, closure>::Value };
			template<char c>
			struct Next {
				enum : int { Value = -1 };
			};
		};
		
		template<typename NFAGraph>
		struct InitialDFANode : EmptyDFANode<
			NFAGraph,
			typename EpsilonClosure<NFAGraph, 0>::Value,
			0
		>{};
		
		template<typename NFA>
		struct InitialDFAGraph {
			typedef NFA NFAGraph;
			enum : int { Size = 1 };
			template<int, typename = void>
			struct Get {};
			template<typename dummy>
			struct Get<0, dummy> {
				struct Value : InitialDFANode<NFA> {};
			};
		};
		
		template<typename DFAGraph, typename Closure>
		class FindDFANode {
			template<int n, bool = n == DFAGraph::Size>
			struct Internal {
				template<bool = SetIsEqual<typename DFAGraph::template Get<n>::Value::Closure, Closure>::Value, typename = void>
				struct Internal1 {
					enum : int { Value = Internal<n + 1>::Value };
				};
				template<typename dummy>
				struct Internal1<true, dummy> {
					enum : int { Value = n };
				};
				enum : int { Value = Internal1<>::Value };
			};
			template<int n>
			struct Internal<n, true> {
				enum : int { Value = -1 };
			};
		public:
			enum : int { Value = Internal<0>::Value };
		};

		template<typename DFAGraph, typename Closure>
		class FindOrCreateDFANode {
			enum : int { Found = FindDFANode<DFAGraph, Closure>::Value };
			template<int = Found, typename = void>
			struct Internal {
				enum : int { Id = Found };
				typedef DFAGraph NewDFAGraph;
			};
			template<typename dummy>
			struct Internal<-1, dummy> {
				enum : int { Id = DFAGraph::Size };
				struct NewDFAGraph : DFAGraph {
					enum : int { Size = DFAGraph::Size + 1 };
					template<int n, bool = n == DFAGraph::Size>
					struct Get {
						struct Value : DFAGraph::template Get<n>::Value {};
					};
					template<int n>
					struct Get<n, true> {
						struct Value : EmptyDFANode<typename DFAGraph::NFAGraph, Closure, DFAGraph::Size> {};
					};
				};
			};
			struct Result : Internal<> {};
		public:
			enum : int { Id = Result::Id };
			typedef typename Result::NewDFAGraph NewDFAGraph;
		};

		template<typename DFAGraph, int id, char c>
		class TargetClosure {
			typedef typename DFAGraph::template Get<id>::Value::Closure NowClosure;
			template<int i, typename PrevResult, bool = i == NowClosure::Size>
			struct Internal {
				enum : int { NowId = NowClosure::template Get<i>::Value };
				typedef typename DFAGraph::NFAGraph::template Get<NowId>::Value NowNode;
				enum : int { NowJump = NowNode::template Next<c>::Value };
				template<bool = (NowJump < 0), typename = void>
				struct Internal2 {
					struct Result : Internal<
						i + 1,
						typename SetUnion<
							typename EpsilonClosure<typename DFAGraph::NFAGraph, NowJump>::Value,
							PrevResult
						>::Result
					>::Result {};
				};
				template<typename dummy>
				struct Internal2<true, dummy> {
					struct Result : Internal<i + 1, PrevResult>::Result {};
				};
				struct Result : Internal2<>::Result {};
			};
			template<int i, typename PrevResult>
			struct Internal<i, PrevResult, true> {
				typedef PrevResult Result;
			};
		public:
			struct Result : Internal<0, EmptySet>::Result {};
		};

		template<typename DFAGraph, int id, char c>
		class PopulateJump {
			typedef typename TargetClosure<DFAGraph, id, c>::Result Target;
			enum : int { TargetSize = Target::Size };
			template<bool = !TargetSize, typename = void>
			struct Internal {
				typedef FindOrCreateDFANode<DFAGraph, Target> Tmp;
				struct Result : public Tmp::NewDFAGraph {
					template<int n, bool = n == id>
					struct Get : public Tmp::NewDFAGraph::template Get<n> {};
					template<int n>
					struct Get<n, true> {
						struct Value : public Tmp::NewDFAGraph::template Get<id>::Value {
							template<char input, bool = input == c>
							struct Next : public Tmp::NewDFAGraph::template Get<id>::Value::template Next<input> {};
							template<char input>
							struct Next<input, true> {
								enum : int { Value = Tmp::Id };
							};
						};
					};
				};
			};
			template<typename dummy>
			struct Internal<true, dummy> {
				typedef DFAGraph Result;
			};
		public:
			struct Result : Internal<>::Result {};
		};

		template<typename DFAGraph, int id>
		class PopulateNode {
			template<int c, typename Prev>
			struct Internal {
				typedef typename Internal<c + 1, typename PopulateJump<Prev, id, c>::Result>::Result Result;
			};
			template<typename Prev>
			struct Internal<CHAR_MAX + 1, Prev> {
				typedef Prev Result;
			};
		public:
			struct Result : Internal<CHAR_MIN, DFAGraph>::Result {};
		};

		template<typename DFAGraph>
		class PopulateDFA {
			template<typename Prev, int i, bool = i == Prev::Size>
			struct Internal {
				struct Result : Internal<typename PopulateNode<Prev, i>::Result, i + 1>::Result {};
			};
			template<typename Prev, int i>
			struct Internal<Prev, i, true> {
				typedef Prev Result;
			};
		public:
			struct Result : Internal<DFAGraph, 0>::Result {};
		};
		
		template<typename NFA>
		struct NFAToDFA : PopulateDFA<InitialDFAGraph<NFA>>::Result {};
	}

	namespace Expr {
		template<typename Char>
		struct Single : public details::NFAGenSingle<Char> {};
		template<typename ...Children>
		struct Sequence : public details::NFAGenChain<Children...> {};
		template<typename Child>
		struct Optional : public details::NFAGenOptional<Child> {};
		template<typename ...Children>
		struct Or : public details::NFAGenOr<Children...> {};
		template<typename Child, int N>
		struct Repeat : public details::NFAGenRepeatFixed<Child, N> {};
		template<typename Child>
		struct Star : public details::NFAGenRepeatAny<Child> {};
		template<typename Child>
		struct Plus : public details::NFAGenRepeatNonZero<Child> {};
		template<typename Child, int N>
		struct Min : public details::NFAGenRepeatMin<Child, N> {};
		template<typename Child, int N>
		struct Max : public details::NFAGenRepeatMax<Child, N> {};
		template<typename Child, int Min, int Max>
		struct MinMax : public details::NFAGenRepeatMinMax<Child, Min, Max> {};
	}

	template<typename Expr>
	struct Compile : public details::NFAToDFA<typename details::NFAFinishGeneration<Expr>::Value> {};

	namespace Runtime {
		template<int ...vals>
		struct LookupTable {
			static int LookUp(int index) {
				static int data[] {vals...};
				return data[index];
			}
		};

		template<int start, int length, int ...vals>
		struct RangedLookupTable {
			static int LookUp(int index) {
				return (index < start || index >= start + length) ? -1 : LookupTable<vals...>::LookUp(index - start);
			}
		};

		struct EmptyLookupTable {
			static int LookUp(int) {
				return -1;
			}
		};
		
		template<typename Node, int now = 0, bool = now == UCHAR_MAX + 1
			|| Node::template Next</* to suppress warning*/(now == UCHAR_MAX + 1) ? 0 : (now + CHAR_MIN)>::Value != -1>
		struct FindLowerRange {
			enum : int { Result = FindLowerRange<Node, now + 1>::Result };
		};
		template<typename Node, int now>
		struct FindLowerRange<Node, now, true> {
			enum : int { Result = now };
		};
		
		template<typename Node, int now = UCHAR_MAX, bool = Node::template Next<now + CHAR_MIN>::Value != -1>
		struct FindUpperRange {
			enum : int { Result = FindUpperRange<Node, now - 1>::Result };
		};
		template<typename Node, int now>
		struct FindUpperRange<Node, now, true> {
			enum : int { Result = now };
		};

		template<typename Node>
		class NodeToTableRaw {
			template<int i, bool, int ...tab>
			struct Internal {
				typedef typename Internal<i + 1, i + 1 == UCHAR_MAX + 1, tab..., Node::template Next<i + CHAR_MIN>::Value>::Result Result;
			};
			template<int i, int ...tab>
			struct Internal<i, true, tab...> {
				typedef LookupTable<tab...> Result;
			};
		public:
			typedef typename Internal<0, false>::Result Result;
		};

		template<typename Node, int start, int length>
		class NodeToRangedTableRaw {
			template<int i, int remain, int ...tab>
			struct Internal {
				typedef typename Internal<i + 1, remain - 1, tab..., Node::template Next<i + start + CHAR_MIN>::Value>::Result Result;
			};
			template<int i, int ...tab>
			struct Internal<i, 0, tab...> {
				typedef RangedLookupTable<start, length, tab...> Result;
			};
		public:
			typedef typename Internal<0, length>::Result Result;
		};

		template<typename Node>
		class GetLookupTableForNode {
			enum : int { LowerRange = FindLowerRange<Node>::Result };
			template<bool = LowerRange == UCHAR_MAX + 1, typename = void>
			struct Internal {
				enum : int { UpperRange = FindUpperRange<Node>::Result };
				template<bool = LowerRange == 0 && UpperRange == UCHAR_MAX, typename = void>
				struct Internal2 {
					typedef typename NodeToRangedTableRaw<Node, LowerRange, UpperRange - LowerRange + 1>::Result Result;
				};
				template<typename dummy>
				struct Internal2<true, dummy> {
					typedef typename NodeToTableRaw<Node>::Result Result;
				};
				typedef typename Internal2<>::Result Result;
			};
			template<typename dummy>
			struct Internal<true, dummy> {
				typedef EmptyLookupTable Result;
			};
		public:
			typedef typename Internal<>::Result Result;
		};

		template<typename DFAGraph>
		class GetLookupFunctionList {
			template<int i, bool, int(*...Now)(int)>
			struct Internal {
				typedef typename Internal<i + 1, i + 1 == DFAGraph::Size, Now...,
					&GetLookupTableForNode<typename DFAGraph::template Get<i>::Value>::Result::LookUp>::Result
				Result;
			};
			template<int i, int(*...Now)(int)>
			struct Internal<i, true, Now...> {
				struct Result {
					static int(*GetTableFor(int x))(int) {
						static int(*tab[])(int) {Now...};
						return tab[x];
					}
				};
			};
		public:
			static int(*Get(int x))(int) {
				return Internal<0, 0 == DFAGraph::Size>::Result::GetTableFor(x);
			}
		};

		template<typename DFAGraph>
		class GetCanAcceptList {
			template<int i, bool, bool ...Now>
			struct Internal {
				typedef typename Internal<i + 1, i + 1 == DFAGraph::Size, Now...,
					DFAGraph::template Get<i>::Value::CanAccept>::Result
				Result;
			};
			template<int i, bool ...Now>
			struct Internal<i, true, Now...> {
				struct Result {
					static bool GetCanAccept(int x) {
						static bool tab[] {Now...};
						return tab[x];
					}
				};
			};
		public:
			static bool Get(int x) {
				return Internal<0, 0 == DFAGraph::Size>::Result::GetCanAccept(x);
			}
		};
	}

	template<typename CompiledExpr>
	class Matcher {
		typedef Runtime::GetLookupFunctionList<CompiledExpr> Jumps;
		typedef Runtime::GetCanAcceptList<CompiledExpr> CanAccepts;
	public:
		template<typename Iter>
		static bool Match(Iter begin, Iter end) {
			static_assert(std::is_same<typename std::iterator_traits<Iter>::value_type, char>::value, "Iterator value should be char type");
			int state{};
			for (; begin != end; begin++) {
				state = Jumps::Get(state)(*begin - CHAR_MIN);
				if (state < 0) {
					return false;
				}
			}
			return CanAccepts::Get(state);
		}
	};
}

#endif
